package com.nodot.web.pair2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.web.pair2.common.Pair2Utils;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class Pair2UtilsTest {
	
	@Autowired
	Pair2Utils utils;
	
	@Test
	public void termToDateTest() {
		String term = "7d";
		log.info("term {} covert to date -> {}",term, utils.termToDate(term));
		
		term = "1m";
		log.info("term {} covert to date -> {}",term, utils.termToDate(term));
		
		term = "6m";
		log.info("term {} covert to date -> {}",term, utils.termToDate(term));
		
		term = "7dsdf";
		log.info("term {} covert to date -> {}",term, utils.termToDate(term));
		
	}

}
