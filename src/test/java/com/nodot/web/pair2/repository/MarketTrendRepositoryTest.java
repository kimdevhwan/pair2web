package com.nodot.web.pair2.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.web.pair2.common.Pair2Utils;
import com.nodot.web.pair2.vo.product.ProductVo;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class MarketTrendRepositoryTest {
	
	@Autowired
	MarketTrendRepository trendRepository;
	
	@Autowired
	Pair2Utils utils;
	
	@Test
	public void marketTrendDataTest() {
		int productIdx = 26;
		String size = "265";
		
		String test = trendRepository.getLastUpdateDate(productIdx); 
		log.info("{}",test);
		
		ProductVo product = new ProductVo();
		product.setLastUpdateDate(test);
		
		trendRepository.getMinMaxAvgPrice(productIdx, "2018-11-20 00:00:00").ifPresent( summary -> {
			product.setMinPrice(Integer.parseInt(summary.get("MIN_PRICE").toString()));
			product.setMaxPrice(Integer.parseInt(summary.get("MAX_PRICE").toString()));
			product.setAvgPrice(Integer.parseInt(summary.get("AVG_PRICE").toString()));
		});
		
		log.info("date : {}, count : {}, min : {}, max : {} , avg : {}",
				product.getLastUpdateDate(),
				product.getAskStockCount(),
				product.getMinPrice(),
				product.getMaxPrice(),
				product.getAvgPrice());
		
		trendRepository.getPriceSummaryByAllSize(productIdx, "2018-11-20 00:00:00").forEach(trend -> {
			log.info("size : {}, count :{}, min : {} , max : {}"
					,trend.get("SIZE")
					,trend.get("COUNT")
					,trend.get("MIN_PRICE")
					,trend.get("MAX_PRICE"));
		});
		
		trendRepository.getPriceSummaryBySize(productIdx, size , "2018-11-20 00:00:00").forEach(perSize -> {
			log.info("size : {}, count :{}, price : {}"
					,perSize.get("SIZE")
					,perSize.get("COUNT")
					,perSize.get("PRICE"));
		});
		
		trendRepository.getTrendForGraph(productIdx, "2018-11-20 00:00:00").forEach(perSize -> {
			log.info("date : {}, min : {}, max : {}, avg : {}"
					,perSize.get("MARKET_DATE")
					,perSize.get("MIN_PRICE")
					,perSize.get("MAX_PRICE")
					,perSize.get("AVG_PRICE"));
		});
		
		trendRepository.getExistSizeByProduct(productIdx, "2018-11-20 00:00:00").forEach(sizes -> {
			log.info("exist size : {}", sizes.get("SIZE"));
		});
		
	}
	
	@Test
	public void existSizeCheckTest() {
		int productIdx = 26;
		utils.sizeExistCheck(trendRepository.getExistSizeByProduct(productIdx, "2018-11-20 00:00:00"))
		.forEach((k,v)-> {
			log.info("exist size {} ? {} ",k,v);
		});
	}

}
