package com.nodot.web.pair2.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.web.pair2.vo.product.ProductVo;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class ProductRepositoryTest {
	
	@Autowired
	ProductRepository productRepository;
	
	@Test
	@Transactional
	public void productFindTest() {
		List<ProductVo> test = productRepository.findAll();
		test.forEach(product -> {
			log.info("제품명 : {}",product.getName());
			product.getImages().forEach(images->{
				log.info("이미지 : {}",images.getImageUrl());
			});
		});
	}
	
}
