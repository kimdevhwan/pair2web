package com.nodot.web.pair2.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.web.pair2.vo.UserRoleVO;
import com.nodot.web.pair2.vo.UserVO;

import lombok.extern.java.Log;

import com.nodot.web.pair2.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class UserRepositoryTest {
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void userInsertTest() {
		UserVO user = new UserVO();
		
		user.setName("name");
		user.setEmail("email1");
		user.setPassword("pass");

		userRepository.save(user);
	}
	
	@Test
	public void userLeaveTest() {
		userRepository.findById(1).ifPresent(user -> {
			user.setLeave(true);
			userRepository.save(user);
		});
	}

	@Transactional
	@Test
	public void userCreateWithRoleTest() {
		String email = "test@"+UUID.randomUUID().toString();
		
		UserVO user = new UserVO();
		user.setName("name");
		user.setEmail(email);
		user.setPassword("pass");
		
		for(int i=1; i<3; i++) {
			UserRoleVO role =  new UserRoleVO();
			role.setRole("ROLE_USER"+i);
			role.setUser(user);
			user.addRole(role);
		}
		
		userRepository.save(user);
		
		
		UserRoleVO role =  new UserRoleVO();
		role.setRole("ROLE_AFTER");
		role.setUser(user);
		user.addRole(role);
		
		assertTrue(Optional.ofNullable(userRepository.findByEmail(email)).isPresent());
		
	}
}
