package com.nodot.web.pair2.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nodot.web.pair2.vo.UserVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserSerivceTest {
	
	@Autowired
	UserService userSerivce;
	
	@Transactional
	@Test
	public void createAccountTest() {
		UserVO user = new UserVO();
		
		String email = "create@account.test"; 
		
		user.setName("test");
		user.setEmail(email);
		user.setPassword("pass");
		
		userSerivce.createAccount(user);
		
		UserVO account = userSerivce.findByEmail(email);
		assertTrue(Optional.ofNullable(account).isPresent());
	}

}
