package com.nodot.web.pair2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pair2WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pair2WebApplication.class, args);
	}
}
