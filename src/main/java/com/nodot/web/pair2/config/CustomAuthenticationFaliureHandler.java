package com.nodot.web.pair2.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.nodot.web.pair2.vo.UserVO;

import lombok.extern.java.Log;

@Log
public class CustomAuthenticationFaliureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		Enumeration<String> test = request.getParameterNames();
		
		while(test.hasMoreElements()) {
			log.info(test.nextElement());
		}
		UserVO user = new UserVO();
		user.setEmail(request.getParameter("email"));
		user.setPassword(request.getParameter("password"));
		String loginRedirectUrl = request.getParameter("loginRedirect");
		
		request.setAttribute("user", user);
		request.setAttribute("loginRedirect", loginRedirectUrl);
		request.setAttribute("loginFailMsg", exception.getMessage());
		log.info(user.getEmail());
		log.info(user.getPassword());
		log.info(loginRedirectUrl);
		log.info(exception.getMessage());
		
		request.getRequestDispatcher("/login?fail=true").forward(request, response);
	};

}
