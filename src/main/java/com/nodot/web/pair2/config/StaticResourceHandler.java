package com.nodot.web.pair2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class StaticResourceHandler extends WebMvcConfigurationSupport {
	
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {"classpath:/static/"};

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
		registry.addResourceHandler("/product/img/**").addResourceLocations("file:/home/admin/img/");
	}
	
	
	

}
