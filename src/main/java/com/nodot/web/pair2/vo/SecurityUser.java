package com.nodot.web.pair2.vo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecurityUser extends User {

	private static final String ROLE_PREFIX = "ROLE_";
	private static final long serialVersionUID = 1L;

	public SecurityUser(UserVO user) {
		super(user.getEmail(), user.getPassword(), makeGrantedAuthority(user.getRoles()));
	}
	
	private static List<GrantedAuthority> makeGrantedAuthority(List<UserRoleVO> roles){
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		roles.forEach(role -> list.add(new SimpleGrantedAuthority(ROLE_PREFIX + role.getRole())));
		return list;
	}

}
