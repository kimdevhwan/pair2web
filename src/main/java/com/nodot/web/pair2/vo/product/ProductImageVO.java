package com.nodot.web.pair2.vo.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "PRODUCT_IMAGES", schema = "PAIR2")
public class ProductImageVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;
	
	@Column(name = "IMAGE_URL", nullable = false)
	private String imageUrl;
	
	@Column(name = "IS_REP", nullable = false)
	private boolean represent = false;
	
	@Column(name = "PRODUCT_IDX", nullable = false)
	private int productIdx;
	
}
