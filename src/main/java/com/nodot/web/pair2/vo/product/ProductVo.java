package com.nodot.web.pair2.vo.product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.nodot.web.pair2.vo.trend.MarketTrendVo;

import lombok.Data;

@Data
@Entity
@Table(name = "PRODUCT", schema = "PAIR2")
public class ProductVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;

	@Column(name = "CATEGORY", nullable = true)
	private String category;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "RETAIL_PRICE", nullable = false)
	private int retailPrice;

	@Column(name = "RELEASE_DATE", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate releaseDate;

	@Column(name = "COLOR", nullable = true)
	private String color;

	@Column(name = "STYLE_CODE", nullable = false)
	private String styleCode;

	@Column(name = "DESCRIPTION", nullable = true)
	private String desc;

	@Column(name = "TAGS", nullable = false)
	private String tags;

	@Column(name = "CLASSIFY_REGEX", nullable = false)
	private String classifyRegex;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "PRODUCT_IDX")
	private List<ProductImageVO> images;
	
	public void addImage(ProductImageVO image) {
		if (images == null) {
			images = new ArrayList<ProductImageVO>();
		}
		images.add(image);
	}
	
	
	@Transient
	private String lastUpdateDate;
	
	@Transient
	private int askStockCount;
	
	@Transient
	private int minPrice;
	
	@Transient
	private int maxPrice;
	
	@Transient
	private int avgPrice;
}
