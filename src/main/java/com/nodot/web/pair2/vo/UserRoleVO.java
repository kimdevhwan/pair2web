package com.nodot.web.pair2.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "user")
@Entity
@Table(name="USER_ROLE")
public class UserRoleVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;
	
	@Column(name = "ROLE")
	private String role;
	
	@ManyToOne
    @JoinColumn(name = "USER_IDX", referencedColumnName="IDX")
    private UserVO user;
	

}
