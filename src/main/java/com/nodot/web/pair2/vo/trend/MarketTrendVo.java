package com.nodot.web.pair2.vo.trend;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "MARKET_TREND", schema = "PAIR2")
public class MarketTrendVo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;
	
	@Column(name = "SIZE", nullable = false)
	private String size;
	
	@Column(name = "PRICE", nullable = false)
	private String price;
	
	@Column(name = "REG_DATE", nullable = false)
	private String regDate;
			
	@Column(name = "PRODUCT_IDX", nullable = false)
	private int productIdx;
	
	private int count;
	private int minPrice;
	private int maxPrice;

}
