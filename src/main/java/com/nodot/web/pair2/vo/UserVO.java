package com.nodot.web.pair2.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name="USER")
public class UserVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "IDX")
	private int idx;
	
	@NotBlank
	@Column(name = "EMAIL", nullable = false, length = 50)
	private String email;
	
	@NotBlank
	@Column(name = "PASSWORD", nullable = false, length = 60)
	private String password;
	
	@NotBlank
	@Column(name = "NAME",nullable = false, length = 10)
	private String name;
	
	@Column(name = "IS_LEAVE", insertable = false)
	private boolean isLeave;
	
	@CreationTimestamp
	@Column(name = "REG_DATE", insertable = false)
	private Date regDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<UserRoleVO> roles;
	
	public void addRole(UserRoleVO role) {
		if(roles == null) {
			roles = new ArrayList<UserRoleVO>();	
		}
		roles.add(role);
	}

}
