package com.nodot.web.pair2.exceptions;

public class EmailDuplicatedException extends RuntimeException {

	private static final long serialVersionUID = -2466684338436680176L;

	public EmailDuplicatedException() {
		super("이미 가입된 이메일 입니다. 이건 서버 메세지");
	}
}
