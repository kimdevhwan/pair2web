package com.nodot.web.pair2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class Pair2ExceptionHandler {
	
	@ResponseBody
	@ResponseStatus(HttpStatus.FOUND)
	@ExceptionHandler(EmailDuplicatedException.class)
	public String duplicateEmailException(EmailDuplicatedException e) {
		return e.getMessage();
	}

}
