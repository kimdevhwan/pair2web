package com.nodot.web.pair2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nodot.web.pair2.service.UserService;
import com.nodot.web.pair2.vo.UserVO;

@Controller
@RequestMapping("/user")
public class UserController {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserService userService;
	
	@PostMapping
	public String createAccount(UserVO user) {
		userService.createAccount(user);
		return "redirect:/";
	}
	
	@ResponseBody
	@GetMapping(value = "/{email}/", produces = MediaType.TEXT_PLAIN_VALUE)
	public String verifyDuplicationEmail(@PathVariable String email) {
		logger.info(email);
		userService.verifyDuplicationEmail(email);
		return "verified";
	}
}
