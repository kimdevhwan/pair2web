package com.nodot.web.pair2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@GetMapping("/")
	public String welcome() {
		return "/welcome";
	}
	
	@RequestMapping("/login")
	public String logIn() {
		return "login";
	}
	
	@GetMapping("/signup")
	public String signUp() {
		return "signup";
	}
	
	@GetMapping("/mypage")
	public String mypage () {
		return "mypage";
	}
	
}
