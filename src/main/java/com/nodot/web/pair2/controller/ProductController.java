package com.nodot.web.pair2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nodot.web.pair2.service.ProductService;
import com.nodot.web.pair2.vo.product.ProductVo;

import lombok.extern.log4j.Log4j2;

@Controller
@RequestMapping("/product")
@Log4j2
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping
	public String productList(Model model,
			@RequestParam(value = "term", required = false, defaultValue = "7d") String term) {
		model.addAttribute("products", productService.productList(term));
		return "productList";
	}
	
	@GetMapping("/detail/{productIdx}")
	public String productDetail(@PathVariable("productIdx") int productIdx, Model model,
			@RequestParam(value = "term", required = false, defaultValue = "7d") String term,
			@RequestParam(value = "size", required = false, defaultValue = "all") String size) {
		
		log.info("idx : {}, term : {}, size : {}", productIdx, term, size);
		
		productService.productDetail(productIdx, term, size).forEach((K,V) ->{
			model.addAttribute(K, V);
		});
		return "productDetail";
	}
	
	@GetMapping("/trend/{productIdx}")
	public String trendInfo(@PathVariable("productIdx") int productIdx, Model model,
			@RequestParam(value = "term", required = false, defaultValue = "7d") String term,
			@RequestParam(value = "size", required = false, defaultValue = "all") String size) {
		
		
		log.info("idx : {}, term : {}, size : {}", productIdx, term, size);
		
		productService.trendInfo(productIdx, term, size).forEach((K,V) ->{
			model.addAttribute(K, V);
		});
		
		String fragment;
		
		if(size.equals("all")) {
			fragment = "trendInfoAllSize";
		} else {
			fragment = "trendInfoPerSize";
		}
		return "fragments/trendTable :: " + fragment;
	}

}