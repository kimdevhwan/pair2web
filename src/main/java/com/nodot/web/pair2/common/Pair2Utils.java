package com.nodot.web.pair2.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.format.DataFormatMatcher;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class Pair2Utils {
	
	public String termToDate(String term) {
		
		String dayTermRegex = "^(\\d+)d$";
		String monthTermRegex = "^(\\d+)m$";
		
		LocalDateTime date = null;
		
		if(term.matches(dayTermRegex)) {
			String termResult = Optional.ofNullable(this.regexResult(dayTermRegex, term)).orElse("7");
			date = LocalDateTime.now().minusDays(Integer.parseInt(termResult));
		} else if(term.matches(monthTermRegex)) {
			String termResult = Optional.ofNullable(this.regexResult(monthTermRegex, term)).orElse("1");
			date = LocalDateTime.now().minusMonths(Integer.parseInt(termResult));
		} else {
			return "all";
		}
		
		return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00"));
	}
	
	private String regexResult(String regex, String text) {
		Matcher matcher= Pattern.compile(regex).matcher(text);
		
		if(matcher.matches())
			return matcher.group(1);
		
		return null;
	}
	
	public Map<Integer, Boolean> sizeExistCheck(List<Map<String, String>> existsizes){
		Map<Integer, Boolean> result = new HashMap<Integer, Boolean>();
		
		for(int i = 230; i<=300; i+=5) {
			for( Map<String, String> size : existsizes) {
				if(i == Integer.parseInt(size.get("SIZE"))) {
					result.put(i, true);
				}
			}
		}
		
		return result;
	}

}
