package com.nodot.web.pair2.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.nodot.web.pair2.vo.UserVO;

public interface UserRepository extends CrudRepository<UserVO, Integer> {
	
	@Query(value = "select * from USER where EMAIL = :email", nativeQuery = true)
	UserVO findByEmail(@Param("email") String email);

}
