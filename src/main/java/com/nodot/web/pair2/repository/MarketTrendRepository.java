package com.nodot.web.pair2.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nodot.web.pair2.vo.trend.MarketTrendVo;

public interface MarketTrendRepository extends JpaRepository<MarketTrendVo, Integer> {
	
	@Query(value = "select \r\n" + 
			"    date_format(max(REG_DATE), '%Y-%m-%d') as LAST_UPDATE_DATE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    PRODUCT_IDX = :idx", nativeQuery = true)
	String getLastUpdateDate(@Param("idx") int productIdx);
	
	@Query(value = "select \r\n" + 
			"    count(IDX)\r\n" + 
			"from\r\n" + 
			"    (select \r\n" + 
			"        IDX\r\n" + 
			"    from\r\n" + 
			"        MARKET_TREND\r\n" + 
			"    where\r\n" + 
			"        PRODUCT_IDX = :idx\r\n" + 
			"            and REG_DATE between :startDate and now()\r\n" + 
			"    group by SELLER_NICK , SIZE) A", nativeQuery = true)
	int getAskSaleCount(@Param("idx") int productIdx, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    count(IDX)\r\n" + 
			"from\r\n" + 
			"    (select \r\n" + 
			"        IDX\r\n" + 
			"    from\r\n" + 
			"        MARKET_TREND\r\n" + 
			"    where\r\n" + 
			"        PRODUCT_IDX = :idx and SIZE = :size\r\n" + 
			"            and REG_DATE between :startDate and now()\r\n" + 
			"    group by SELLER_NICK , SIZE) A", nativeQuery = true)
	int getAskSaleCountBySize(@Param("idx") int productIdx, @Param("size") String size, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    min(PRICE) as MIN_PRICE,\r\n" + 
			"    max(PRICE) as MAX_PRICE,\r\n" + 
			"    round(avg(PRICE), - 3) as AVG_PRICE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    IDX in (select \r\n" + 
			"            max(IDX)\r\n" + 
			"        from\r\n" + 
			"            MARKET_TREND\r\n" + 
			"        where\r\n" + 
			"            PRODUCT_IDX = :idx and PRICE > 0\r\n" + 
			"                and REG_DATE between :startDate and now()\r\n" + 
			"        group by SELLER_NICK , SIZE)", nativeQuery = true)
	Optional<Map<String, Object>> getMinMaxAvgPrice(@Param("idx") int productIdx, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    min(PRICE) as MIN_PRICE,\r\n" + 
			"    max(PRICE) as MAX_PRICE,\r\n" + 
			"    round(avg(PRICE), - 3) as AVG_PRICE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    IDX in (select \r\n" + 
			"            max(IDX)\r\n" + 
			"        from\r\n" + 
			"            MARKET_TREND\r\n" + 
			"        where\r\n" + 
			"            PRODUCT_IDX = :idx and PRICE > 0 and SIZE = :size\r\n" + 
			"                and REG_DATE between :startDate and now()\r\n" + 
			"        group by SELLER_NICK , SIZE)", nativeQuery = true)
	Optional<Map<String, Object>> getMinMaxAvgPriceBySize(@Param("idx") int productIdx, @Param("size") String size, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    SIZE,\r\n" + 
			"    count(0) as COUNT,\r\n" + 
			"    min(PRICE) as MIN_PRICE,\r\n" + 
			"    max(PRICE) as MAX_PRICE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    IDX in (select \r\n" + 
			"            max(IDX)\r\n" + 
			"        from\r\n" + 
			"            MARKET_TREND\r\n" + 
			"        where\r\n" + 
			"            PRODUCT_IDX = :idx and PRICE > 0\r\n" + 
			"                and REG_DATE between :startDate and now()\r\n" + 
			"        group by SELLER_NICK , SIZE)\r\n" + 
			"group by SIZE\r\n" + 
			"order by SIZE", nativeQuery = true)
	List<Map<String, Object>> getPriceSummaryByAllSize(@Param("idx") int productIdx, @Param("startDate") String startDate);
	
	
	@Query(value = "select \r\n" + 
			"    SIZE, count(0) as COUNT, PRICE, REG_DATE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    IDX in (select \r\n" + 
			"            max(IDX)\r\n" + 
			"        from\r\n" + 
			"            MARKET_TREND\r\n" + 
			"        where\r\n" + 
			"            PRODUCT_IDX = :idx and SIZE = :size\r\n" + 
			"                and PRICE > 0\r\n" +	 
			"                and REG_DATE between :startDate and now()\r\n" + 
			"        group by SELLER_NICK , SIZE)\r\n" + 
			"group by SIZE, PRICE\r\n" + 
			"order by SIZE\r\n" + 
			";", nativeQuery = true)
	List<Map<String, Object>> getPriceSummaryBySize(@Param("idx") int productIdx, @Param("size") String size, @Param("startDate") String startDate);
	
	
	@Query(value = "select \r\n" + 
			"    date_format(REG_DATE, '%Y.%m.%d') AS MARKET_DATE,\r\n" + 
			"    min(PRICE) as MIN_PRICE,\r\n" + 
			"    max(PRICE) as MAX_PRICE,\r\n" + 
			"    round(avg(PRICE), - 3) as AVG_PRICE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    PRODUCT_IDX = :idx and PRICE > 0\r\n" + 
			"        and REG_DATE between :startDate and now()\r\n" + 
			"group by MARKET_DATE", nativeQuery = true)
	List<Map<String, Object>> getTrendForGraph(@Param("idx") int productIdx, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    date_format(REG_DATE, '%Y.%m.%d') as MARKET_DATE,\r\n" + 
			"    min(PRICE) as MIN_PRICE,\r\n" + 
			"    max(PRICE) as MAX_PRICE,\r\n" + 
			"    round(avg(PRICE), - 3) as AVG_PRICE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    IDX in (select \r\n" + 
			"            max(IDX)\r\n" + 
			"        from\r\n" + 
			"            MARKET_TREND\r\n" + 
			"        where\r\n" + 
			"            PRODUCT_IDX = :idx and price > 0  and SIZE = :size\r\n" + 
			"                and REG_DATE between :startDate and now()\r\n" + 
			"        group by SELLER_NICK , SIZE)\r\n" + 
			"group by MARKET_DATE", nativeQuery = true)
	List<Map<String, Object>> getTrendForGraphBySize(@Param("idx") int productIdx, @Param("size") String size, @Param("startDate") String startDate);
	
	@Query(value = "select \r\n" + 
			"    SIZE\r\n" + 
			"from\r\n" + 
			"    MARKET_TREND\r\n" + 
			"where\r\n" + 
			"    PRODUCT_IDX = :idx\r\n" + 
			"        and REG_DATE between :startDate and now()\r\n" + 
			"group by SIZE;", nativeQuery = true)
	List<Map<String, String>> getExistSizeByProduct(@Param("idx") int productIdx, @Param("startDate") String startDate);
	
}
