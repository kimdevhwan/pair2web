package com.nodot.web.pair2.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nodot.web.pair2.vo.product.ProductVo;

public interface ProductRepository extends JpaRepository<ProductVo, Integer> {
	
}
