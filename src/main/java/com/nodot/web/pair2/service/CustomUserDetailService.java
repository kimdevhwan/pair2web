package com.nodot.web.pair2.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.nodot.web.pair2.repository.UserRepository;
import com.nodot.web.pair2.vo.SecurityUser;

import lombok.extern.java.Log;

@Service
@Log
public class CustomUserDetailService implements UserDetailsService {
	
	@Autowired
	UserRepository userRepository;
	
	@Transactional
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return Optional.ofNullable(userRepository.findByEmail(email))
				.filter( f -> f != null)
				.map(m -> new SecurityUser(m))
				.get();
	}

}
