package com.nodot.web.pair2.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.nodot.web.pair2.exceptions.EmailDuplicatedException;
import com.nodot.web.pair2.repository.UserRepository;
import com.nodot.web.pair2.vo.SecurityUser;
import com.nodot.web.pair2.vo.UserRoleVO;
import com.nodot.web.pair2.vo.UserVO;

@Service
public class UserService {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserRepository userRepository;
	
	@Transactional
	public void createAccount(UserVO user) {
		Optional.ofNullable(user).ifPresent(account -> {
			PasswordEncoder pwEndcoder = new BCryptPasswordEncoder();
			account.setPassword(pwEndcoder.encode(account.getPassword()));
			
			UserRoleVO role = new UserRoleVO();
			role.setRole("USER");
			role.setUser(account);
			account.addRole(role);
			
			userRepository.save(account);
		});
	}
	
	public void verifyDuplicationEmail(String email) {
		Optional.ofNullable(userRepository.findByEmail(email)).ifPresent(user -> {
			throw new EmailDuplicatedException();
		});
	}
	
	public UserVO findByEmail(String email) {
		UserVO account = userRepository.findByEmail(email);
		return account;
	}


}
