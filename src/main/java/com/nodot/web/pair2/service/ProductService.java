package com.nodot.web.pair2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nodot.web.pair2.repository.ProductRepository;
import com.nodot.web.pair2.common.Pair2Utils;
import com.nodot.web.pair2.repository.MarketTrendRepository;
import com.nodot.web.pair2.vo.product.ProductVo;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	MarketTrendRepository trendRepository;
	
	@Autowired
	Pair2Utils pair2Utils;
	
	@Transactional
	public List<ProductVo> productList(String term) {
		List<ProductVo>	products = productRepository.findAll();
		
		return products.stream()
			.map(product -> {
				trendRepository.getMinMaxAvgPrice(product.getIdx(), pair2Utils.termToDate(term))
					.filter(summary -> summary.get("MIN_PRICE") != null)
					.ifPresent(summary -> {
						product.setMinPrice(Integer.parseInt(summary.get("MIN_PRICE").toString()));
						product.setMaxPrice(Integer.parseInt(summary.get("MAX_PRICE").toString()));
						product.setAvgPrice(Integer.parseInt(summary.get("AVG_PRICE").toString()));
					});
				return product;
			})
			.filter(product -> {
				if(product.getMinPrice() > 0)
					return true;
				else 
					return false;
			})
			.collect(Collectors.toList());
	}
	
	@Transactional
	public Map<String, Object> productDetail(int productIdx, String term, String size) {
		
		ProductVo product = productRepository.findById(productIdx).orElse(new ProductVo());
		
		//최근 시세 적용 일자
		product.setLastUpdateDate(trendRepository.getLastUpdateDate(productIdx));
		
		//기간별 매물 개수
		product.setAskStockCount(trendRepository.getAskSaleCount(productIdx, pair2Utils.termToDate(term)));
		
		//기간별 최고/최저/평균 시세
		trendRepository.getMinMaxAvgPrice(productIdx, pair2Utils.termToDate(term)).ifPresent( summary -> {
			product.setMinPrice(Integer.parseInt(summary.get("MIN_PRICE").toString()));
			product.setMaxPrice(Integer.parseInt(summary.get("MAX_PRICE").toString()));
			product.setAvgPrice(Integer.parseInt(summary.get("AVG_PRICE").toString()));
		});
		
		Map<String, Object> detail = new HashMap<String, Object>();
		detail.put("product", product);
		
		//시세 테이블 데이터
		detail.put("tableData", trendRepository.getPriceSummaryByAllSize(productIdx, pair2Utils.termToDate(term)));
		detail.put("chartData", trendRepository.getTrendForGraph(productIdx, pair2Utils.termToDate(term)));
		
		//존재 하는 매물 사이즈 목록
		detail.put("existSize", pair2Utils
				.sizeExistCheck(trendRepository.getExistSizeByProduct(productIdx, pair2Utils.termToDate(term))));
		//검색 조건
		detail.put("term", term);
		detail.put("size", size);
		
		return detail;
	}
	
	public Map<String, Object> trendInfo(int productIdx, String term, String size) {
		Map<String, Object> trend = new HashMap<String, Object>();
		ProductVo product = new ProductVo();
		
		if(size.equals("all")) {
			//기간별 매물 개수
			product.setAskStockCount(trendRepository.getAskSaleCount(productIdx, pair2Utils.termToDate(term)));
			
			//기간별 최고/최저/평균 시세
			trendRepository.getMinMaxAvgPrice(productIdx, pair2Utils.termToDate(term)).ifPresent( summary -> {
				product.setMinPrice(Integer.parseInt(summary.get("MIN_PRICE").toString()));
				product.setMaxPrice(Integer.parseInt(summary.get("MAX_PRICE").toString()));
				product.setAvgPrice(Integer.parseInt(summary.get("AVG_PRICE").toString()));
			});
			trend.put("product", product);
			trend.put("tableData", trendRepository.getPriceSummaryByAllSize(productIdx, pair2Utils.termToDate(term)));
			trend.put("chartData", trendRepository.getTrendForGraph(productIdx, pair2Utils.termToDate(term)));
		} else {
			//기간별 매물 개수
			product.setAskStockCount(trendRepository.getAskSaleCountBySize(productIdx, size, pair2Utils.termToDate(term)));
			
			//기간별 최고/최저/평균 시세
			trendRepository.getMinMaxAvgPriceBySize(productIdx, size, pair2Utils.termToDate(term)).ifPresent( summary -> {
				product.setMinPrice(Integer.parseInt(summary.get("MIN_PRICE").toString()));
				product.setMaxPrice(Integer.parseInt(summary.get("MAX_PRICE").toString()));
				product.setAvgPrice(Integer.parseInt(summary.get("AVG_PRICE").toString()));
			});
			trend.put("product", product);
			trend.put("tableData", trendRepository.getPriceSummaryBySize(productIdx, size, pair2Utils.termToDate(term)));
			trend.put("chartData", trendRepository.getTrendForGraphBySize(productIdx, size, pair2Utils.termToDate(term)));
		}
		
		
		return trend;
	}

}
