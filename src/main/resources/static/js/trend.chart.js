/**
 * 시세 그래프 스크립트
 */

var chartCanvas = document.getElementById("canvas").getContext('2d');

var chartData = {
	labels : chartDate,
	datasets : [ {
		label : "최고가",
		lineTension : 0,
		fill : false,
		borderColor : '#ff5531',
		backgroundColor : '#ffac9a',
		borderWidth : 1.5,
		pointRadius : 0,
		pointHoverRadius : 5,
		pointHitRadius : 30,
		pointStyle : 'circle',
		data : chartMaxPrice
	}, {
		label : "평균가",
		lineTension : 0,
		fill : false,
		borderColor : '#000000',
		backgroundColor : '#808080',
		borderWidth : 1.5,
		pointRadius : 0,
		pointHoverRadius : 5,
		pointHitRadius : 30,
		pointStyle : 'circle',
		data : chartAvgPrice
	}, {
		label : "최저가",
		lineTension : 0,
		fill : false,
		borderColor : '#238da7',
		backgroundColor : '#71c3d7',
		borderWidth : 1.5,
		pointRadius : 0,
		pointHoverRadius : 5,
		pointHitRadius : 30,
		pointStyle : 'circle',
		data : chartMinPrice
	} ]
};

var chartOptions = {
		scaleGridLineWidth: 0,
		maintainAspectRatio: false,
		legend: {
			position : 'bottom',
		},
		responsive : true,
		tooltips : {
			mode : 'index',
			intersect : false,
			callbacks: {
               label: function(tooltipItem, data) {
            	   labelTitle = data.datasets[tooltipItem.datasetIndex].label;
            	   price = tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            	   return labelTitle+' : '+price;
               }, 
           },
		},
		scales : {
			xAxes : [ {
				type : 'time',
				time : {
					unit : "day",
					displayFormats: {
                        day: 'MM-DD'
                    },
                    tooltipFormat: "YY-MM-DD"
				},
				gridLines : {
					display : false
				}
			} ],
			yAxes : [ {
				ticks : {
					stepSize : 100000,
                    callback: function(value, index, values) {
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
				}
			} ]
		}

};
function drawChart() {
	Chart.defaults.LineWithLine = Chart.defaults.line;
	Chart.controllers.LineWithLine = Chart.controllers.line.extend({
	   draw: function(ease) {
	      Chart.controllers.line.prototype.draw.call(this, ease);

	      if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
	         var activePoint = this.chart.tooltip._active[0],
	             ctx = this.chart.ctx,
	             x = activePoint.tooltipPosition().x,
	             topY = this.chart.scales['y-axis-0'].top,
	             bottomY = this.chart.scales['y-axis-0'].bottom;

	         // draw line
	         ctx.save();
	         ctx.beginPath();
	         ctx.moveTo(x, topY);
	         ctx.lineTo(x, bottomY);
	         ctx.lineWidth = 1;
	         ctx.strokeStyle = '#d2d2d2';
	         ctx.stroke();
	         ctx.restore();
	      }
	   }
	});
	
	lineChart = new Chart(chartCanvas, {
		type : 'LineWithLine',
		data : chartData,
		options : chartOptions
	});
}

window.onload = function() {
	drawChart();
}
