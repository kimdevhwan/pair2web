// Scroll move
function scrollMove(t,h) {
	"use strict";
	if(h==undefined) h=0;
	var o = jQuery('html, body');
	o.animate({
		scrollTop:jQuery(t).offset().top-h
	},500);
}

// Menu Open
function menuOpen(o){
	"use strict";
	var o = $(o).attr('id'),
		a = -$(window).scrollTop();
	$('#wrap').css('top',a).attr('data-top',a);
	$('#'+o).before('<a class="dim dim-'+o+'" onclick="menuClose('+o+');"><i class="sr-only">close</i></a>');
	$('body').addClass('nav-open');
	setTimeout(function  () {
		$('#'+o).show(0,function(){
			$('body').addClass(o+'-open');
		});
	},50);
}

// Menu Close
function menuClose(o){
	'use strict';
	var o = $(o).attr('id'),
		originScroll = -parseInt($('#wrap').attr('data-top'));
	$('body').removeClass(o+'-open');
	setTimeout(function(){
		$('#'+o).hide();
		$('body').removeClass('nav-open').find('.dim').remove();
		$(window).scrollTop(originScroll);
		$('#wrap').removeAttr('style data-top');
	},300);
}


jQuery(function ($) {
	"use strict";
	var body = $('body'),
		$window = $(window);

	$('.js-btn-mn').click(function  () {
		if (body.hasClass('gnb-open')) {
			menuClose(gnb);
		}else {
			menuOpen(gnb);
		}
	});

	$('.js-btn-srch').click(function  () {
		if (body.hasClass('gnb-open')) {
			body.removeClass('gnb-open').find('.dim-gnb').remove();
		}
		menuOpen(hdSrch);
	});

	$('.depth>a').click(function  () {
		var t = $(this);
		t.parent().toggleClass('active');
		t.next().stop().slideToggle();
		return false;
	});
});

